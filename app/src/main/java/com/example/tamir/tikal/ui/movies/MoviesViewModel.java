package com.example.tamir.tikal.ui.movies;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;

import com.example.tamir.tikal.model.Movie;
import com.example.tamir.tikal.repository.MoviesRepository;

import java.util.List;

public class MoviesViewModel extends AndroidViewModel {

    private int pastVisibleItems, visibleItemCount, totalItemCount;
    private MoviesRepository moviesRepository;
    private LiveData<List<Movie>> allMovies;

    public MoviesViewModel(Application application) {
        super(application);
        moviesRepository = new MoviesRepository(application); //could be injected
        allMovies = moviesRepository.getAllMovies();
    }


    public LiveData<List<Movie>> getAllMovies() {
        return allMovies;
    }

    public void fetchMovies() {
        moviesRepository.fetchMovies();
    }

    public int getPastVisibleItems() {
        return pastVisibleItems;
    }

    public void setPastVisibleItems(int pastVisibleItems) {
        this.pastVisibleItems = pastVisibleItems;
    }

    public int getVisibleItemCount() {
        return visibleItemCount;
    }

    public void setVisibleItemCount(int visibleItemCount) {
        this.visibleItemCount = visibleItemCount;
    }

    public int getTotalItemCount() {
        return totalItemCount;
    }

    public void setTotalItemCount(int totalItemCount) {
        this.totalItemCount = totalItemCount;
    }

    public void disposeCompositeDisposable() {
        moviesRepository.disposeCompositeDisposable();
    }

    public void updateRecyclerViewValues(LinearLayoutManager linearLayoutManager) {
        setVisibleItemCount(linearLayoutManager.getChildCount());
        setPastVisibleItems(linearLayoutManager.findFirstVisibleItemPosition());
        setTotalItemCount(linearLayoutManager.getItemCount());
        determineInfiniteScrollAction();
    }

    private void determineInfiniteScrollAction() {
        int visiblePlusPast = getVisibleItemCount() + getPastVisibleItems();
        if (visiblePlusPast >= getTotalItemCount()) {
            fetchMovies();
        }
    }
}