package com.example.tamir.tikal.ui.movie;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.tamir.tikal.R;
import com.example.tamir.tikal.TikalApplication;
import com.example.tamir.tikal.model.Movie;
import com.example.tamir.tikal.ui.movies.MoviesActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieActivity extends AppCompatActivity {

    @BindView(R.id.iv_movie)
    ImageView ivMovie;

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.tv_description)
    TextView tvDescription;

    public static Intent getIntent(Context context, Movie movie) {
        Intent intent = new Intent(context,MovieActivity.class);
        intent.putExtra("description",movie.getOverview());
        intent.putExtra("title",movie.getTitle());
        intent.putExtra("image",movie.getPosterPath());
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);

        ButterKnife.bind(this);

        Intent intent = getIntent();
        String title = intent.getStringExtra("title");
        String description = intent.getStringExtra("description");
        String image = intent.getStringExtra("image");

        String baseUrl = TikalApplication.getAppContext().getString(R.string.image_url);
        String path = baseUrl+image;
        Glide.with(this).load(path).into(ivMovie);

        tvTitle.setText(title);
        tvDescription.setText(description);
    }
}
