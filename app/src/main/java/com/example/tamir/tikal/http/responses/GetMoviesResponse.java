package com.example.tamir.tikal.http.responses;

import com.example.tamir.tikal.model.Movie;

import java.util.List;

public class GetMoviesResponse {

    private int page;
    private int total_pages;
    private List<Movie> results;

    public int getPage() {
        return page;
    }

    public int getTotalPages() {
        return total_pages;
    }

    public List<Movie> getResults() {
        return results;
    }
}
