package com.example.tamir.tikal.repository;


import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.os.Looper;
import android.util.Log;

import com.example.tamir.tikal.R;
import com.example.tamir.tikal.TikalApplication;
import com.example.tamir.tikal.dao.MovieDao;
import com.example.tamir.tikal.http.retrofit.MoviesAPI;
import com.example.tamir.tikal.http.retrofit.RetrofitClient;
import com.example.tamir.tikal.model.Movie;

import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class MoviesRepository {

    private MovieDao movieDao;
    private LiveData<List<Movie>> allMovies;

    private Retrofit retrofitClient;
    private MoviesAPI moviesAPI;
    private CompositeDisposable compositeDisposable;
    private int currentPage = 1;

    public MoviesRepository(Application application) {
        MoviesDatabase database = MoviesDatabase.getInstance(application);

        movieDao = database.movieDao();
        allMovies = movieDao.getAllMovies();

        retrofitClient = RetrofitClient.getInstance();
        moviesAPI = retrofitClient.create(MoviesAPI.class);
        compositeDisposable = new CompositeDisposable();
    }


    public void fetchMovies() {

        compositeDisposable.add(moviesAPI.getMovies(currentPage,TikalApplication.getAppContext().getString(R.string.api_key))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(moviesResponse -> {
                            currentPage++;
                            List<Movie> movies = moviesResponse.getResults();
                            insertAllMovies(movies);
                        }, throwable -> {
                            Log.d("CompositeDisposable",throwable.getMessage());
                        }));

    }

    public void insertAllMovies(final List<Movie> movies) {

        Completable.fromAction(() -> {
            movieDao.insertAllMovies(movies);
        }).subscribeOn(Schedulers.io()).subscribe();

    }

    public LiveData<List<Movie>> getAllMovies() {
        return allMovies;
    }

    public void disposeCompositeDisposable() {
        compositeDisposable.dispose();
    }

}