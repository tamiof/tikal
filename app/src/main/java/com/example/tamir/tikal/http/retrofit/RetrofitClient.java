package com.example.tamir.tikal.http.retrofit;


import android.app.Application;
import android.content.res.Resources;

import com.example.tamir.tikal.R;
import com.example.tamir.tikal.TikalApplication;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private static Retrofit instance;

    public static Retrofit getInstance() {
        if (instance == null) {

            String url = TikalApplication.getAppContext().getString(R.string.base_url);
            instance = new Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }
        return instance;
    }
}
