package com.example.tamir.tikal.ui.movies;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.tamir.tikal.R;
import com.example.tamir.tikal.ui.movie.MovieActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MoviesActivity extends AppCompatActivity {

    private LinearLayoutManager linearLayoutManager;
    private MoviesAdapter adapter;
    private MoviesViewModel moviesViewModel;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movies);
        ButterKnife.bind(this);

        initViewModel();
        initRecyclerView();
        initAdapter();
    }

    private void initViewModel() {
        moviesViewModel = ViewModelProviders.of(this).get(MoviesViewModel.class);
        moviesViewModel.getAllMovies().observe(this, movies -> {
            if (movies != null && !movies.isEmpty()) {
                adapter.setMovies(movies);
            } else {
                moviesViewModel.fetchMovies();
            }

        });
    }

    private void initRecyclerView() {
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    moviesViewModel.updateRecyclerViewValues(linearLayoutManager);
                }
            }
        });
    }

    private void initAdapter() {
        adapter = new MoviesAdapter();
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(movie -> startActivity(MovieActivity.getIntent(MoviesActivity.this,movie)));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        moviesViewModel.disposeCompositeDisposable();
    }
}
