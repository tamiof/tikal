package com.example.tamir.tikal.http.retrofit;


import com.example.tamir.tikal.http.responses.GetMoviesResponse;


import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MoviesAPI {

    @GET("discover/movie")
    Observable<GetMoviesResponse> getMovies(@Query("page") int page,
                                            @Query("api_key") String apiKey);
}
