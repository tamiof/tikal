package com.example.tamir.tikal.ui.movies;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.tamir.tikal.R;
import com.example.tamir.tikal.TikalApplication;
import com.example.tamir.tikal.model.Movie;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MoviesHolder> {

    private OnItemClickListener listener;
    private List<Movie> movies = new ArrayList<>();

    @NonNull
    @Override
    public MoviesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_item, parent, false);

        return new MoviesHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MoviesHolder holder, int position) {

        Movie movie = movies.get(position);

        String baseUrl = TikalApplication.getAppContext().getString(R.string.image_url);
        String path = baseUrl+movie.getPosterPath();
        Glide.with(holder.ivMovie.getContext()).load(path).into(holder.ivMovie);

    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public void setMovies(List<Movie> movies) {
        int beforeSize = this.movies.size();
        this.movies.addAll(movies);
        notifyItemRangeInserted(beforeSize,this.movies.size());
    }

    class MoviesHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv)
        ImageView ivMovie;

        public MoviesHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(v -> {
                int position = getAdapterPosition();
                if (listener != null && position != RecyclerView.NO_POSITION) {
                    listener.onItemClick(movies.get(position));
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Movie movie);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}