package com.example.tamir.tikal;

import android.app.Application;
import android.content.Context;

public class TikalApplication extends Application {

    private static TikalApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static Context getAppContext() {
        return instance;
    }

}
